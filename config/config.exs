# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :global_reader,
  ecto_repos: [GlobalReader.Repo]

# Configures the endpoint
config :global_reader, GlobalReaderWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "IMOsiBYjAgUJ433xicEGs5ef2msVPZxe/7X10jvOwdhFYfZ6AgWunqg+WCjgHZTn",
  render_errors: [view: GlobalReaderWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: GlobalReader.PubSub,
  live_view: [signing_salt: "jSvj797v"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Bamboo emails
config :global_reader, GlobalReader.Mailer, adapter: Bamboo.LocalAdapter

# Http Client
config :global_reader, http_client: GlobalReader.HttpClient

# Healthcheck reporting
config :global_reader, healthcheck_reporting: true

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
