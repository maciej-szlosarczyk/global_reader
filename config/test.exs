use Mix.Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :global_reader, GlobalReader.Repo,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  database: "global_reader_test#{System.get_env("MIX_TEST_PARTITION")}",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :global_reader, GlobalReaderWeb.Endpoint,
  http: [port: 4001],
  server: true

# Print only warnings and errors during test
config :logger, level: :warn

# Use Hound for acceptance tests
config :hound, driver: "chrome_driver", browser: "chrome_headless"

# Bamboo emails
config :global_reader, GlobalReader.Mailer, adapter: Bamboo.TestAdapter

# Http Client
config :global_reader, http_client: GlobalReader.HttpClientMock

# Automatic healthcheck reporting is disabled in test
config :global_reader, healthcheck_reporting: false
