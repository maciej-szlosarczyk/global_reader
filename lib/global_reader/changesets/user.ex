defmodule GlobalReader.Changesets.User do
  @moduledoc """
  Changeset for user records.
  """

  use Ecto.Schema

  alias Ecto.Changeset
  alias GlobalReader.Changesets.User

  schema("users") do
    field(:email, :string)
    field(:preferred_name, :string)
    field(:password, :string, virtual: true)
    field(:password_hash, :string)
    field(:password_confirmation, :string, virtual: true)

    field(:uuid, :binary_id, read_after_writes: true)
    timestamps(type: :utc_datetime)
  end

  @spec create_changeset(map()) :: %Ecto.Changeset{}
  def create_changeset(attrs) when is_map(attrs) do
    user = %User{}

    user
    |> Changeset.cast(attrs, [:email, :preferred_name, :password_confirmation, :password])
    |> Changeset.validate_required([:email, :preferred_name, :password, :password_confirmation])
    |> Changeset.validate_format(:email, ~r/.+@.+/, message: "must be a valid email address")
    |> Changeset.validate_length(:password, min: 12)
    |> validate_password()
    |> put_password_hash()
    |> Changeset.unique_constraint(:uuid)
    |> Changeset.unique_constraint(:email)
  end

  def update_changeset(%User{} = user, attrs) when is_map(attrs) do
    user
    |> Changeset.cast(attrs, [:preferred_name, :password_confirmation, :password])
    |> Changeset.validate_length(:password, min: 12)
    |> validate_password()
    |> put_password_hash()
  end

  @spec validate_password(%Ecto.Changeset{}) :: %Ecto.Changeset{}
  defp validate_password(%Ecto.Changeset{} = changeset) do
    password_confirmation = Changeset.get_change(changeset, :password_confirmation)

    Changeset.validate_change(changeset, :password, fn :password, password ->
      if password == password_confirmation do
        []
      else
        [password: "must be the same as password confirmation"]
      end
    end)
  end

  @spec put_password_hash(%Ecto.Changeset{}) :: %Ecto.Changeset{}
  defp put_password_hash(
         %Ecto.Changeset{valid?: true, changes: %{password: password}} = changeset
       ) do
    Changeset.change(changeset, Bcrypt.add_hash(password))
    |> Changeset.delete_change(:password)
    |> Changeset.delete_change(:password_confirmation)
  end

  defp put_password_hash(changeset), do: changeset
end
