defmodule GlobalReader.Changesets.PasswordReset do
  @moduledoc """
  Changeset for password resets.
  """

  use Ecto.Schema
  alias Ecto.Changeset
  alias GlobalReader.Changesets.PasswordReset

  schema("password_resets") do
    field(:valid_until, :utc_datetime, null: false)
    field(:used_at, :utc_datetime, null: true)

    belongs_to(
      :user,
      GlobalReader.Changesets.User,
      references: :uuid,
      type: :binary_id,
      foreign_key: :user_uuid
    )

    field(:uuid, :binary_id, read_after_writes: true)
    timestamps(type: :utc_datetime)
  end

  @spec create_changeset(map()) :: Ecto.Changeset.t()
  def create_changeset(attrs) when is_map(attrs) do
    password_reset = %PasswordReset{}

    password_reset
    |> Changeset.cast(attrs, [:user_uuid, :valid_until])
    |> Changeset.validate_required(:user_uuid)
    |> Changeset.validate_required(:valid_until)
    |> Changeset.foreign_key_constraint(:user_uuid)
  end

  @spec mark_as_used_changeset(%PasswordReset{}, map()) :: Ecto.Changeset.t()
  def mark_as_used_changeset(%PasswordReset{} = password_reset, attrs) when is_map(attrs) do
    password_reset
    |> Changeset.cast(attrs, [:used_at])
    |> Changeset.validate_required(:used_at)
  end
end
