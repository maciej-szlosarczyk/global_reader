defmodule GlobalReader.Repo do
  use Ecto.Repo,
    otp_app: :global_reader,
    adapter: Ecto.Adapters.Postgres
end
