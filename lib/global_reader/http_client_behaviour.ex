defmodule GlobalReader.HttpClientBehaviour do
  @moduledoc """
  Minimal Contract for any synchronous HTTP client.
  """

  @typep url :: binary()
  @typep options :: Keyword.t()
  @typep body :: {:form, [{atom(), any()}]}
  @typep headers :: [{atom(), binary()}] | [{binary(), binary()}]
  @typep method :: :get | :head | :post | :delete | :connect | :options | :trace | :patch
  @typep result :: %{:status_code => non_neg_integer(), :body => binary(), :headers => headers()}

  @callback request(method(), url(), headers(), body(), options()) ::
              {:ok, result()} | {:error, any()}
end
