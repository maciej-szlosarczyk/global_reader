defmodule GlobalReader.HttpClient do
  @moduledoc """
  Live implementation of HTTPClient. In practice, a thin wrapper around hackney.
  """

  @behaviour GlobalReader.HttpClientBehaviour

  @impl true
  def request(method, url, headers, body, options) do
    case :hackney.request(method, url, headers, body, options) do
      {return, status, headers, client_ref} ->
        {:ok, body} = :hackney.body(client_ref)
        {return, %{status_code: status, body: body, headers: headers}}

      {:error, error} ->
        {:error, error}
    end
  end
end
