defmodule GlobalReader.PasswordResets do
  @moduledoc """
  Creating and manipulating password reset requests.
  """

  alias GlobalReader.Changesets.{PasswordReset, User}
  alias GlobalReader.Repo

  import Ecto.Query, only: [from: 2]

  @utc "Etc/UTC"
  @thirty_minutes_in_seconds 1800

  @doc """
  By default, makes the request valid for another 30 minutes since creation.
  """
  @spec create(%{uuid: binary()} | %User{}) ::
          {:ok, %PasswordReset{}} | {:error, Ecto.Changeset.t()}
  def create(user) do
    {:ok, now} = DateTime.now(@utc)
    valid_until = DateTime.add(now, @thirty_minutes_in_seconds, :second)
    create(user.uuid, valid_until)
  end

  @spec create(binary(), DateTime.t()) :: {:ok, %PasswordReset{}} | {:error, Ecto.Changeset.t()}
  def create(user_uuid, valid_until) do
    password_reset =
      PasswordReset.create_changeset(%{user_uuid: user_uuid, valid_until: valid_until})

    Repo.insert(password_reset)
  end

  @spec find_valid_by_uuid(binary()) :: %PasswordReset{} | nil
  def find_valid_by_uuid(uuid) do
    Repo.one(valid_query(uuid))
  end

  @spec find_valid_by_uuid!(binary()) :: %PasswordReset{} | nil
  def find_valid_by_uuid!(uuid) do
    Repo.one!(valid_query(uuid))
  end

  @spec mark_as_used(%PasswordReset{}) ::
          {:ok, %PasswordReset{}}
          | {:error, %PasswordReset{}}
          | {:error, binary(), %PasswordReset{}}
  def mark_as_used(%PasswordReset{} = password_reset) do
    {:ok, now} = DateTime.now(@utc)

    password_reset
    |> PasswordReset.mark_as_used_changeset(%{used_at: now})
    |> Repo.update()
  end

  @doc """
  Used only for testing now, might be useful later.
  """
  @spec for_email(binary()) :: [%PasswordReset{}]
  def for_email(email) do
    query =
      from p in PasswordReset,
        join: u in User,
        on: p.user_uuid == u.uuid,
        where: u.email == ^email

    Repo.all(query)
  end

  defp valid_query(uuid) do
    {:ok, now} = DateTime.now(@utc)

    from p in PasswordReset,
      where: p.uuid == ^uuid,
      where: p.valid_until >= ^now,
      where: is_nil(p.used_at),
      preload: [:user]
  end
end
