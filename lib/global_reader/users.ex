defmodule GlobalReader.Users do
  @moduledoc """
  Functions for creating and deleting users.
  """
  alias GlobalReader.Changesets.User
  alias GlobalReader.Repo

  import Ecto.Query, only: [from: 2]

  @spec prepare_create(map()) :: %Ecto.Changeset{}
  def prepare_create(data) when is_map(data) do
    User.create_changeset(data)
  end

  @spec prepare_update(%User{}, map()) :: %Ecto.Changeset{}
  def prepare_update(%User{} = user, data) do
    User.update_changeset(user, data)
  end

  @spec create(map()) :: {:ok, %User{}} | {:error, %Ecto.Changeset{}}
  def create(data) when is_map(data) do
    user = User.create_changeset(data)
    Repo.insert(user)
  end

  @spec find_by_email(String.t()) :: %User{} | nil
  def find_by_email(email) when is_bitstring(email) do
    query = from u in User, where: [email: ^email], select: u
    Repo.one(query)
  end

  @spec find_by_email!(String.t()) :: %User{}
  def find_by_email!(email) when is_bitstring(email) do
    query = from u in User, where: [email: ^email], select: u
    Repo.one!(query)
  end

  @spec find_by_id(integer()) :: %User{} | nil
  def find_by_id(id) when is_integer(id) do
    query = from u in User, where: [id: ^id], select: u
    Repo.one(query)
  end

  @spec find_by_uuid(<<_::288>>) :: %User{} | nil
  def find_by_uuid(<<_::64, 45::8, _::32, 45::8, _::32, 45::8, _::32, 45::8, _::96>> = uuid) do
    query = from u in User, where: [uuid: ^uuid], select: u
    Repo.one(query)
  end

  def find_by_uuid(_) do
    nil
  end

  @spec update(%User{}, map()) :: {:ok, %User{}} | {:error, %Ecto.Changeset{}}
  def update(%User{} = user, attrs) do
    User.update_changeset(user, attrs) |> Repo.update()
  end

  @spec delete(%User{}) :: {:ok, %User{}} | {:error, %Ecto.Changeset{}}
  def delete(%User{} = user) do
    Repo.delete(user)
  end

  # By design, it always returns "invalid password" error, it is a good
  # practice to not expose emails or usernames of registered users, we
  # never know who is actually submitting the form, can be a bot.
  @spec check_email_and_password(String.t(), String.t()) :: {:ok, %User{}} | {:error, String.t()}
  def check_email_and_password(email, password)
      when is_bitstring(email) and is_bitstring(password) do
    user = find_by_email(email)
    check_password(user, password)
  end

  @spec check_password(%User{} | nil, term()) :: {:ok, %User{}} | {:error, String.t()}
  defp check_password(%User{} = user, password) do
    Bcrypt.check_pass(user, password)
  end

  defp check_password(nil, _) do
    {:error, "invalid password"}
  end
end
