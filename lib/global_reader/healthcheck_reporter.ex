defmodule GlobalReader.HealthcheckReporter do
  @moduledoc """
  GenServer for checking out the state of GlobalReader servers.
  """

  use GenServer

  require Logger

  require Record
  Record.defrecord(:state, timer: nil)
  @type state :: record(:state, timer: nil | :timer.tref())

  @thirty_seconds 30 * 1000
  @url "https://apis.globalreader.eu/health"
  # There is a wildcard cert, requires depth checking.
  @connection_options [ssl_options: [depth: 10]]
  @http_client Application.get_env(:global_reader, :http_client)

  @spec start_link(any()) :: {:ok, pid()}
  def start_link(_) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  # GenServer callbacks
  @spec init(any()) :: {:ok, state}
  def init(_opts) do
    case Application.get_env(:global_reader, :healthcheck_reporting, true) do
      false ->
        {:ok, state()}

      true ->
        {:ok, timer_reference} = :timer.send_interval(@thirty_seconds, :check)
        {:ok, state(timer: timer_reference)}
    end
  end

  @spec handle_call(any(), {pid(), any()}, state()) :: {:stop, :not_implemented, state()}
  def handle_call(_, _, existing_state) do
    {:stop, :not_implemented, existing_state}
  end

  @spec handle_info(:check, state()) :: {:noreply, state}
  def handle_info(:check, existing_state) do
    start = :erlang.monotonic_time(:millisecond)
    result = @http_client.request(:get, @url, [], "", @connection_options)
    stop = :erlang.monotonic_time(:millisecond)
    duration = stop - start

    case result do
      {:error, error} ->
        Logger.error(
          "#{__MODULE__} reports error request: #{error}. Request duration: #{duration} ms."
        )

      {:ok, %{status_code: status, body: body}} ->
        case duration do
          x when x > 400 ->
            msg =
              Enum.join([
                "#{__MODULE__} reports slow request.",
                "Request duration: #{x} ms. Body: #{body}. Status code: #{status}"
              ])

            Logger.error(msg)

          x ->
            msg =
              Enum.join([
                "#{__MODULE__} report.",
                "Request duration: #{x} ms. Body: #{body}. Status code: #{status}"
              ])

            Logger.warn(msg)
        end
    end

    {:noreply, existing_state}
  end

  @spec terminate(:normal | :shutdown | {:shutdown, any()}, state()) :: :ok
  def terminate(_reason, state(timer: nil) = _existing_state) do
    :ok
  end

  def terminate(_reason, state(timer: timer) = _existing_state) do
    {:ok, :cancel} = :timer.cancel(timer)
    :ok
  end
end
