defmodule GlobalReader.Emails do
  import Bamboo.Email
  alias GlobalReaderWeb.Router.Helpers, as: Routes

  def reset_password_email(user, password_reset) do
    new_email(
      to: user.email,
      from: "test@test.com",
      subject: "Reset your password",
      html_body: """
      <h2>Reset your password</h2>
      <p>You can reset your password by clicking this link:</p>
      <p>
        <a href="#{
        Routes.password_reset_edit_url(GlobalReaderWeb.Endpoint, :edit, password_reset.uuid)
      }">
        Reset your password
        </a>
      </p>
      """,
      text_body: """
      Reset your password
      You can reset your password by clicking this link:

      #{Routes.password_reset_edit_url(GlobalReaderWeb.Endpoint, :edit, password_reset.uuid)}
      """
    )
  end
end
