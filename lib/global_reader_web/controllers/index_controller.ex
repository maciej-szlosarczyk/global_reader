defmodule GlobalReaderWeb.IndexController do
  use GlobalReaderWeb, :controller

  def index(conn, _) do
    conn
    |> render("index.html")
  end
end
