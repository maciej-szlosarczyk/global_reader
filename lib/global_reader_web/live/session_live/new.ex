defmodule GlobalReaderWeb.SessionLive.New do
  use GlobalReaderWeb, :live_view

  alias GlobalReader.Users

  def mount(_params, _session, socket) do
    session = %{email: nil, password: nil}

    {:ok, assign(socket, session: session, error: nil)}
  end

  def render(assigns) do
    GlobalReaderWeb.SessionView.render("new.html", assigns)
  end

  # By design, there is no actual session created, this just checks
  # password and email combination. There is no need for that for the
  # purpose of this task.
  def handle_event("save", %{"email" => email, "password" => password}, socket) do
    case Users.check_email_and_password(email, password) do
      {:error, error_text} ->
        {:noreply, assign(socket, error: error_text)}

      {:ok, user} ->
        {:noreply,
         socket
         |> put_flash(:info, "You have been signed in.")
         |> redirect(to: Routes.user_show_path(socket, :show, user.email))}
    end
  end
end
