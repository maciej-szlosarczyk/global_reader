defmodule GlobalReaderWeb.PasswordResetLive.Edit do
  use GlobalReaderWeb, :live_view

  alias GlobalReader.{PasswordResets, Users}
  alias Phoenix.LiveView.Socket

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    GlobalReaderWeb.PasswordResetView.render("edit.html", assigns)
  end

  def handle_params(%{"uuid" => uuid}, _url, socket) do
    {:noreply, socket |> assign(uuid: uuid) |> fetch()}
  end

  def handle_event("validate", %{"user" => user_params}, %Socket{assigns: %{user: user}} = socket) do
    changeset =
      Users.prepare_update(user, user_params)
      |> Map.put(:action, :update)

    {:noreply, assign(socket, changeset: changeset)}
  end

  def handle_event(
        "save",
        %{"user" => user_params},
        %Socket{assigns: %{user: user, password_reset: password_reset}} = socket
      ) do
    case Users.update(user, user_params) do
      {:error, changeset} ->
        {:noreply, assign(socket, changeset: changeset)}

      {:ok, user} ->
        case PasswordResets.mark_as_used(password_reset) do
          {:ok, _} ->
            {:noreply,
             socket
             |> put_flash(:info, "Your password has been updated.")
             |> redirect(to: Routes.user_show_path(socket, :show, user.email))}
        end
    end
  end

  defp fetch(%Socket{assigns: %{uuid: uuid}} = socket) do
    pr = PasswordResets.find_valid_by_uuid!(uuid)
    user = pr.user

    assign(socket, password_reset: pr, user: user, changeset: Users.prepare_update(user, %{}))
  end
end
