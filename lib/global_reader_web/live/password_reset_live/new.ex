defmodule GlobalReaderWeb.PasswordResetLive.New do
  use GlobalReaderWeb, :live_view

  alias GlobalReader.Changesets.User
  alias GlobalReader.{PasswordResets, Users}

  @error_message "An error has occured, please try again later"

  def mount(_params, _session, socket) do
    {:ok, assign(socket, email: nil, error: nil)}
  end

  def render(assigns) do
    GlobalReaderWeb.PasswordResetView.render("new.html", assigns)
  end

  def handle_event("save", %{"email" => email}, socket) do
    user = Users.find_by_email(email)

    case user do
      nil ->
        {:noreply,
         socket
         |> put_flash(:info, message(email))
         |> redirect(to: Routes.index_path(socket, :index))}

      %User{} = user ->
        case PasswordResets.create(user) do
          {:ok, pr} ->
            GlobalReader.Emails.reset_password_email(user, pr)
            |> GlobalReader.Mailer.deliver_later()

            {:noreply,
             socket
             |> put_flash(:info, message(email))
             |> redirect(to: Routes.index_path(socket, :index))}

          {:error, _changeset} ->
            {:noreply, assign(socket, error: @error_message)}
        end
    end
  end

  defp message(email) do
    "If user #{email} exists, a one-time password reset link has been sent. The link is valid for 30 minutes."
  end
end
