defmodule GlobalReaderWeb.UserLive.New do
  use GlobalReaderWeb, :live_view

  alias GlobalReader.Users

  def mount(_params, _session, socket) do
    changeset = Users.prepare_create(%{})
    {:ok, assign(socket, changeset: changeset)}
  end

  def render(assigns) do
    GlobalReaderWeb.UserView.render("new.html", assigns)
  end

  def handle_event("validate", %{"user" => user_params}, socket) do
    changeset =
      Users.prepare_create(user_params)
      |> Map.put(:action, :insert)

    {:noreply, assign(socket, changeset: changeset)}
  end

  def handle_event("save", %{"user" => user_params}, socket) do
    case Users.create(user_params) do
      {:ok, user} ->
        {:noreply,
         socket
         |> put_flash(:info, "User has been created.")
         |> redirect(to: Routes.user_show_path(socket, :show, user.email))}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
