defmodule GlobalReaderWeb.UserLive.Show do
  use GlobalReaderWeb, :live_view

  alias GlobalReader.Users
  alias Phoenix.LiveView.Socket

  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    GlobalReaderWeb.UserView.render("show.html", assigns)
  end

  def handle_params(%{"email" => email}, _url, socket) do
    {:noreply, socket |> assign(email: email) |> fetch()}
  end

  defp fetch(%Socket{assigns: %{email: email}} = socket) do
    assign(socket, user: Users.find_by_email!(email))
  end
end
