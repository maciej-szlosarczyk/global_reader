# GlobalReader

Test application for GlobalReader.

## Language versions

Uses at the time of writing latest versions of Erlang (23), Elixir (1.10) and
LTS NodeJS (12). More details in [.tool-versions](.tool-versions) file, which
can be picked up by asdf version manager.

## Database

Uses PostgreSQL 11, with client CLI being 12.

## Testing

There are some UI tests written with help of Hound, they use chromedriver.
However, live view tests with Chromedriver are really slow so they are hidden
behind `slow` tag and disabled by default. To run them, use:

```
$ chromedriver & echo $! > chromedriver.pid # starts a chromedriver process
$ mix test --include slow:true
$ kill `cat chromedriver.pid` && rm chromedriver.pid
```
