defmodule GlobalReaderWeb.BrowserCase do
  @moduledoc """
  This test defines basic conveniences for testing in a full browser
  """

  use ExUnit.CaseTemplate
  alias Ecto.Adapters.SQL.Sandbox

  using do
    quote do
      # Import conveniences for testing with connections
      import Plug.Conn
      import Phoenix.ConnTest
      import GlobalReaderWeb.ConnCase

      use Hound.Helpers

      alias GlobalReaderWeb.Router.Helpers, as: Routes

      # The default endpoint for testing
      @endpoint GlobalReaderWeb.Endpoint
    end
  end

  setup(tags) do
    :ok = Sandbox.checkout(GlobalReader.Repo)

    unless tags[:async] do
      Sandbox.mode(GlobalReader.Repo, {:shared, self()})
    end

    metadata = Phoenix.Ecto.SQL.Sandbox.metadata_for(GlobalReader.Repo, self())

    Hound.start_session(
      metadata: metadata,
      additional_capabilities: %{
        chromeOptions: %{
          "args" => [
            "--device-agent=#{Hound.Browser.user_agent(:chrome)}",
            "--headless",
            "--disable-gpu",
            "--window-size=1400,1400"
          ]
        }
      }
    )

    {:ok, conn: Phoenix.ConnTest.build_conn()}
  end
end
