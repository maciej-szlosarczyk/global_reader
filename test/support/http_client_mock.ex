defmodule GlobalReader.HttpClientMock do
  @moduledoc """
  HTTP client mock to use in test so that network requests are never actually made.
  """

  @behaviour GlobalReader.HttpClientBehaviour

  @impl true
  def request(_method, _url, headers, body, _options) do
    opts = Application.get_env(:global_reader, :http_client_opts, [])

    # Create artificial failure
    fail = Keyword.get(opts, :fail, false)

    # Create artificial delay
    timeout = Keyword.get(opts, :timeout, 300)
    :timer.sleep(timeout)

    if fail do
      {:error, :asked_to_fail}
    else
      {:ok, %{status_code: 200, body: body, headers: headers}}
    end
  end

  def client_opts do
    Application.get_env(:global_reader, :http_client_opts, [])
  end
end
