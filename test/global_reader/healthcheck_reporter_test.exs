defmodule GlobalReader.HealthcheckReporterTest do
  use ExUnit.Case, async: true
  import Mox
  import ExUnit.CaptureLog

  setup :verify_on_exit!

  describe "reporting" do
    test "logs warning on success" do
      log =
        capture_log(fn ->
          GlobalReader.HttpClientMock
          |> allow(self(), :erlang.whereis(GlobalReader.HealthcheckReporter))
          |> expect(:request, fn :get,
                                 "https://apis.globalreader.eu/health",
                                 [],
                                 "",
                                 [ssl_options: [depth: 10]] ->
            :timer.sleep(100)
            {:ok, %{status_code: 200, body: "OK", headers: []}}
          end)

          :erlang.send(GlobalReader.HealthcheckReporter, :check)

          # Wait for the cycle to complete
          :timer.sleep(300)
        end)

      assert log =~ "[warn] Elixir.GlobalReader.HealthcheckReporter report"
      assert log =~ "Body: OK. Status code: 200"
    end

    test "logs warning when result is failure, but within time" do
      log =
        capture_log(fn ->
          GlobalReader.HttpClientMock
          |> allow(self(), :erlang.whereis(GlobalReader.HealthcheckReporter))
          |> expect(:request, fn :get,
                                 "https://apis.globalreader.eu/health",
                                 [],
                                 "",
                                 [ssl_options: [depth: 10]] ->
            :timer.sleep(100)
            {:ok, %{status_code: 404, body: "Not Found", headers: []}}
          end)

          :erlang.send(GlobalReader.HealthcheckReporter, :check)

          # Wait for the cycle to complete
          :timer.sleep(300)
        end)

      assert log =~ "[warn] Elixir.GlobalReader.HealthcheckReporter report"
      assert log =~ "Body: Not Found. Status code: 404"
    end

    test "logs error when request takes long time" do
      log =
        capture_log(fn ->
          GlobalReader.HttpClientMock
          |> allow(self(), :erlang.whereis(GlobalReader.HealthcheckReporter))
          |> expect(:request, fn :get,
                                 "https://apis.globalreader.eu/health",
                                 [],
                                 "",
                                 [ssl_options: [depth: 10]] ->
            :timer.sleep(500)
            {:ok, %{status_code: 200, body: "OK", headers: []}}
          end)

          :erlang.send(GlobalReader.HealthcheckReporter, :check)

          # Wait for the cycle to complete
          :timer.sleep(600)
        end)

      assert log =~ "[error] Elixir.GlobalReader.HealthcheckReporter reports slow request"
      assert log =~ "Body: OK. Status code: 200"
    end

    test "logs error when request fails" do
      log =
        capture_log(fn ->
          GlobalReader.HttpClientMock
          |> allow(self(), :erlang.whereis(GlobalReader.HealthcheckReporter))
          |> expect(:request, fn :get,
                                 "https://apis.globalreader.eu/health",
                                 [],
                                 "",
                                 [ssl_options: [depth: 10]] ->
            {:error, "I was asked to fail"}
          end)

          :erlang.send(GlobalReader.HealthcheckReporter, :check)

          # Wait for the cycle to complete
          :timer.sleep(100)
        end)

      assert log =~
               "[error] Elixir.GlobalReader.HealthcheckReporter reports error request: I was asked to fail"
    end
  end
end
