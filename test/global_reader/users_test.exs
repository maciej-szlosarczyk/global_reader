defmodule GlobalReader.UsersTest do
  use GlobalReader.DataCase

  alias GlobalReader.Users

  @valid_user %{
    email: "test@test.com",
    password: "test_password_long",
    password_confirmation: "test_password_long",
    preferred_name: "Test User"
  }

  describe "prepare_create/1" do
    test "returns a create changeset" do
      result = Users.prepare_create(@valid_user)
      assert result.valid?
    end
  end

  describe "create/1 on clean run" do
    test "inserts user when valid" do
      {status, user} = Users.create(@valid_user)

      assert :ok == status
      assert @valid_user.email == user.email
      assert @valid_user.preferred_name == user.preferred_name
      assert user.uuid
    end

    test "does not insert when invalid" do
      {status, _user} = Users.create(%{})

      assert :error == status
    end
  end

  describe "create/1 constraints" do
    setup do
      {:ok, user} = Users.create(@valid_user)
      {:ok, %{user: user}}
    end

    test "email must be unique" do
      {status, user} = Users.create(@valid_user)
      assert :error == status
      refute user.valid?
    end
  end

  describe "find functions" do
    setup do
      {:ok, user} = Users.create(@valid_user)
      {:ok, %{user: user}}
    end

    test "find_by_id/1 success", %{user: user} do
      result = Users.find_by_id(user.id)
      assert result == user
    end

    test "find_by_id/1 failure", %{user: user} do
      id = user.id + 1
      result = Users.find_by_id(id)
      refute result
    end

    test "find_by_email/1 success", %{user: user} do
      result = Users.find_by_email(@valid_user.email)
      assert result == user
    end

    test "find_by_email/1 failure" do
      result = Users.find_by_email("Random text")
      refute result
    end

    test "find_by_email!/1 success", %{user: user} do
      result = Users.find_by_email!(@valid_user.email)
      assert result == user
    end

    test "find_by_email!/1 failure" do
      assert_raise(Ecto.NoResultsError, fn ->
        Users.find_by_email!("")
      end)
    end
  end

  describe "check_email_and_password/2" do
    setup do
      {:ok, user} = Users.create(@valid_user)
      {:ok, %{user: user}}
    end

    test "returns ok tuple when valid", %{user: user} do
      result = Users.check_email_and_password(@valid_user.email, @valid_user.password)
      assert {:ok, user} == result
    end

    test "returns error tuple when invalid" do
      result = Users.check_email_and_password(@valid_user.email, "Random text")
      assert {:error, "invalid password"} == result
    end

    test "returns error tuple when user does not exist" do
      result = Users.check_email_and_password("Invalid", "Invalid")
      assert {:error, "invalid password"} == result
    end
  end
end
