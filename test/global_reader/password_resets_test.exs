defmodule GlobalReader.PasswordResetsTest do
  use GlobalReader.DataCase

  alias GlobalReader.{PasswordResets, Users}

  @valid_user %{
    email: "test@test.com",
    password: "test_password_long",
    password_confirmation: "test_password_long",
    preferred_name: "Test User"
  }

  @random_uuid "6cda17ff-53eb-4be4-9cd0-8a52b19a7317"

  setup do
    {:ok, user} = Users.create(@valid_user)
    {:ok, %{user: user}}
  end

  describe "create/1" do
    test "does not create anything when user does not exist" do
      {:error, changeset} = PasswordResets.create(%{uuid: @random_uuid})
      refute changeset.valid?
      assert [] == PasswordResets.for_email("random@email.com")
    end

    test "creates object when user exists", %{user: user} do
      {result, changeset} = PasswordResets.create(user)
      assert :ok == result

      for_email = PasswordResets.for_email(user.email)
      assert 1 == length(for_email)
      assert [changeset] == for_email
    end
  end

  describe "mark_as_used/1" do
    setup(%{user: user}) do
      {:ok, reset} = PasswordResets.create(user)
      {:ok, %{reset: reset}}
    end

    test "sets reset", %{reset: reset} do
      refute reset.used_at
      {:ok, new_reset} = PasswordResets.mark_as_used(reset)

      assert reset.uuid == new_reset.uuid
      assert new_reset.used_at
    end
  end

  describe "find_valid_by_uuid/1" do
    setup(%{user: user}) do
      {:ok, reset} = PasswordResets.create(user)
      {:ok, %{reset: reset}}
    end

    test "finds valid ones only", %{reset: reset} do
      {:ok, _new_reset} = PasswordResets.mark_as_used(reset)
      refute PasswordResets.find_valid_by_uuid(reset.uuid)
    end
  end
end
