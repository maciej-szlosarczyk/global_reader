defmodule GlobalReader.Changesets.UserTest do
  use GlobalReader.DataCase, async: true

  alias GlobalReader.Changesets.User
  alias GlobalReader.Users

  @valid_user %{
    email: "test@test.com",
    password: "test_password_long",
    password_confirmation: "test_password_long",
    preferred_name: "Test User"
  }

  describe "create_changeset/1" do
    test "valid data" do
      changeset = User.create_changeset(@valid_user)
      assert changeset.valid?
    end

    test "missing name" do
      changeset = User.create_changeset(%{})
      assert "can't be blank" in errors_on(changeset).preferred_name
    end

    test "missing email" do
      changeset = User.create_changeset(%{})
      assert "can't be blank" in errors_on(changeset).email
    end

    test "invalid email" do
      changeset = User.create_changeset(%{email: "Visibly not an email"})
      assert "must be a valid email address" in errors_on(changeset).email
    end

    test "missing password" do
      changeset = User.create_changeset(%{})
      assert "can't be blank" in errors_on(changeset).password
    end

    test "password too short" do
      changeset = User.create_changeset(%{password: "1"})
      assert "should be at least 12 character(s)" in errors_on(changeset).password
    end

    test "password does not match confirmation" do
      changeset = User.create_changeset(%{password: "1", password_confirmation: "2"})
      assert "must be the same as password confirmation" in errors_on(changeset).password
    end
  end

  describe "update_changeset/2" do
    setup do
      {:ok, user} = Users.create(@valid_user)
      {:ok, %{user: user}}
    end

    test "valid data", %{user: user} do
      update_data = %{
        preferred_name: "Updated User",
        password: "updated_long_password",
        password_confirmation: "updated_long_password"
      }

      changeset = User.update_changeset(user, update_data)
      assert changeset.valid?
    end

    test "preferred_name blank", %{user: user} do
      changeset = User.update_changeset(user, %{preferred_name: 1})
      assert "is invalid" in errors_on(changeset).preferred_name
    end

    test "password too short", %{user: user} do
      changeset = User.update_changeset(user, %{password: "1"})
      assert "should be at least 12 character(s)" in errors_on(changeset).password
    end

    test "password does not match confirmation", %{user: user} do
      changeset = User.update_changeset(user, %{password: "1", password_confirmation: "2"})
      assert "must be the same as password confirmation" in errors_on(changeset).password
    end
  end
end
