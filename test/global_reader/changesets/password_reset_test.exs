defmodule GlobalReader.Changesets.PasswordResetTest do
  use GlobalReader.DataCase, async: true

  alias GlobalReader.Changesets.PasswordReset
  alias GlobalReader.Users

  @valid_user %{
    email: "test@test.com",
    password: "test_password_long",
    password_confirmation: "test_password_long",
    preferred_name: "Test User"
  }

  @utc "Etc/UTC"

  setup do
    {:ok, user} = Users.create(@valid_user)
    {:ok, %{user: user}}
  end

  describe "create_changeset/1" do
    test "missing user uuid" do
      changeset = PasswordReset.create_changeset(%{})
      assert "can't be blank" in errors_on(changeset).user_uuid
    end

    test "missing valid_until", %{user: user} do
      changeset = PasswordReset.create_changeset(%{user_uuid: user.uuid})
      assert "can't be blank" in errors_on(changeset).valid_until
    end

    test "valid data, no foreign key check", %{user: user} do
      {:ok, valid_until} = DateTime.now(@utc)

      changeset =
        PasswordReset.create_changeset(%{user_uuid: user.uuid, valid_until: valid_until})

      assert changeset.valid?
    end
  end
end
