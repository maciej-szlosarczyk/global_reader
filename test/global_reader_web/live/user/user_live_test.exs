defmodule GlobalReaderWeb.UserLive.UserLiveTest do
  use GlobalReaderWeb.ConnCase

  import Phoenix.LiveViewTest
  alias GlobalReader.Users

  @valid_user %{
    email: "test@test.com",
    password: "test_password_long",
    password_confirmation: "test_password_long",
    preferred_name: "Test User"
  }

  describe "/users/new" do
    test "creates a user by submitting the form", %{conn: conn} do
      refute GlobalReader.Users.find_by_email("tester@test.com")

      {:ok, view, _html} = live(conn, Routes.user_new_path(@endpoint, :new))

      {:ok, show_view} =
        view
        |> element("form")
        |> render_submit(
          user: %{
            preferred_name: "Tester",
            email: "tester@test.com",
            password: "Long_Password123",
            password_confirmation: "Long_Password123"
          }
        )
        |> follow_redirect(conn, "/users/tester%40test.com")

      {:ok, show_view, _} = live(show_view)

      assert render(show_view) =~ "User has been created."
      assert GlobalReader.Users.find_by_email("tester@test.com")
    end

    test "shows error when not valid", %{conn: conn} do
      refute GlobalReader.Users.find_by_email("tester@test.com")

      {:ok, view, _html} = live(conn, Routes.user_new_path(@endpoint, :new))

      show_view =
        view
        |> element("form")
        |> render_submit(
          user: %{
            preferred_name: "Tester",
            password: "Long_Password123",
            password_confirmation: "Long_Password123"
          }
        )

      assert show_view =~
               "<span class=\"invalid-feedback\" phx-feedback-for=\"user_email\">can&apos;t be blank</span>"

      refute GlobalReader.Users.find_by_email("tester@test.com")
    end
  end

  describe "users/:email" do
    setup do
      {:ok, user} = Users.create(@valid_user)
      {:ok, %{user: user}}
    end

    test "displays it when user exist", %{user: user, conn: conn} do
      {:ok, view, _html} = live(conn, Routes.user_show_path(@endpoint, :show, user.email))

      assert render(view) =~ user.uuid
      assert render(view) =~ user.preferred_name
    end

    test "raises when user does not exist", %{conn: conn} do
      assert_raise(Ecto.NoResultsError, fn ->
        live(conn, Routes.user_show_path(@endpoint, :show, "not-email"))
      end)
    end
  end
end
