defmodule GlobalReaderWeb.SessionLive.SessionLiveTest do
  use GlobalReaderWeb.ConnCase

  import Phoenix.LiveViewTest
  alias GlobalReader.Users

  @valid_user %{
    email: "test@test.com",
    password: "test_password_long",
    password_confirmation: "test_password_long",
    preferred_name: "Test User"
  }

  describe "/sessions/new" do
    setup do
      {:ok, user} = Users.create(@valid_user)
      {:ok, %{user: user}}
    end

    test "shows error when user does not exist", %{conn: conn} do
      {:ok, view, _html} = live(conn, Routes.session_new_path(@endpoint, :new))

      result_view =
        view
        |> element("form")
        |> render_submit(%{email: "Random", password: "Random"})

      assert result_view =~
               "<span class=\"invalid-feedback\">invalid password</span>"
    end

    test "shows error when user password is invalid", %{conn: conn, user: user} do
      {:ok, view, _html} = live(conn, Routes.session_new_path(@endpoint, :new))

      result_view =
        view
        |> element("form")
        |> render_submit(%{email: user.email, password: "Random"})

      assert result_view =~
               "<span class=\"invalid-feedback\">invalid password</span>"
    end

    test "signs in when all is valid", %{conn: conn} do
      {:ok, view, _html} = live(conn, Routes.session_new_path(@endpoint, :new))

      {:ok, show_view} =
        view
        |> element("form")
        |> render_submit(%{email: @valid_user.email, password: @valid_user.password})
        |> follow_redirect(conn, "/users/test%40test.com")

      {:ok, show_view, _} = live(show_view)
      assert render(show_view) =~ "You have been signed in."
    end
  end
end
