defmodule GlobalReaderWeb.PasswordResetLive.PasswordResetTest do
  use GlobalReaderWeb.ConnCase

  import Phoenix.LiveViewTest
  alias GlobalReader.{PasswordResets, Users}

  @valid_user %{
    email: "test@test.com",
    password: "test_password_long",
    password_confirmation: "test_password_long",
    preferred_name: "Test User"
  }

  describe "/password_resets/new" do
    setup do
      {:ok, user} = Users.create(@valid_user)
      {:ok, %{user: user}}
    end

    test "creates record when user exists", %{conn: conn, user: user} do
      assert PasswordResets.for_email(user.email) |> Enum.empty?()

      {:ok, view, _html} = live(conn, Routes.password_reset_new_path(@endpoint, :new))

      {:ok, result_view} =
        view
        |> element("form")
        |> render_submit(%{email: user.email})
        |> follow_redirect(conn, "/")

      assert result_view.resp_body =~
               "If user test@test.com exists, a one-time password reset link has been sent. The link is valid for 30 minutes."

      refute PasswordResets.for_email(user.email) |> Enum.empty?()
    end

    test "has the same behaviour when user does not exists", %{conn: conn} do
      {:ok, view, _html} = live(conn, Routes.password_reset_new_path(@endpoint, :new))

      {:ok, result_view} =
        view
        |> element("form")
        |> render_submit(%{email: "does-not-exist@test.com"})
        |> follow_redirect(conn, "/")

      assert result_view.resp_body =~
               "If user does-not-exist@test.com exists, a one-time password reset link has been sent. The link is valid for 30 minutes."
    end
  end

  describe "/password_resets/:uuid" do
    setup do
      {:ok, user} = Users.create(@valid_user)
      {:ok, pr} = PasswordResets.create(user)
      {:ok, %{user: user, password_reset: pr}}
    end

    test "can be accessed via a link and filled", %{user: user, password_reset: pr, conn: conn} do
      {:ok, view, _html} = live(conn, Routes.password_reset_edit_path(@endpoint, :edit, pr.uuid))

      {:ok, show_view} =
        view
        |> element("form")
        |> render_submit(
          user: %{
            password: "Very_new_Password123",
            password_confirmation: "Very_new_Password123"
          }
        )
        |> follow_redirect(conn, "/users/test%40test.com")

      {:ok, show_view, _} = live(show_view)
      assert render(show_view) =~ "Your password has been updated."

      {status, updated_user} =
        GlobalReader.Users.check_email_and_password("test@test.com", "Very_new_Password123")

      assert :ok == status
      assert user.uuid == updated_user.uuid
    end

    test "shows errors when present", %{password_reset: pr, conn: conn} do
      {:ok, view, _html} = live(conn, Routes.password_reset_edit_path(@endpoint, :edit, pr.uuid))

      show_view =
        view
        |> element("form")
        |> render_submit(
          user: %{
            password: "Very_new_Password123",
            password_confirmation: "Other"
          }
        )

      assert show_view =~
               "<span class=\"invalid-feedback\" phx-feedback-for=\"user_password\">must be the same as password confirmation</span>"

      {status, _updated_user} =
        GlobalReader.Users.check_email_and_password("test@test.com", "Very_new_Password123")

      assert :error == status
    end

    test "cannot be accessed when password reset is used", %{password_reset: pr, conn: conn} do
      {:ok, _} = PasswordResets.mark_as_used(pr)

      assert_raise(Ecto.NoResultsError, fn ->
        live(conn, Routes.password_reset_edit_path(@endpoint, :edit, pr.uuid))
      end)
    end
  end
end
