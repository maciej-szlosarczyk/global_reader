defmodule GlobalReaderWeb.Browser.UsersTest do
  use GlobalReaderWeb.BrowserCase

  alias GlobalReader.Users

  @valid_user %{
    email: "test@test.com",
    password: "test_password_long",
    password_confirmation: "test_password_long",
    preferred_name: "Test User"
  }

  # Live-view tests need a long, one second timeout before they get actually rendered in Chrome.
  # For this reason, they should not be executed by default, but only if you put the slow
  # tag in included.
  @tag slow: true
  describe "/users/new" do
    test "succeededs with right parameters" do
      navigate_to(Routes.user_new_path(@endpoint, :new))

      find_element(:id, "user_preferred_name") |> fill_field("Tester")
      find_element(:id, "user_email") |> fill_field("tester@test.com")
      find_element(:id, "user_password") |> fill_field("Long_Password_123")
      find_element(:id, "user_password_confirmation") |> fill_field("Long_Password_123")
      find_element(:id, "user_submit") |> click()

      :timer.sleep(1000)
      assert page_source() =~ "User has been created"
    end
  end

  @tag slow: true
  describe "/users/show" do
    setup do
      {:ok, user} = Users.create(@valid_user)
      {:ok, %{user: user}}
    end

    test "shows user details page", %{user: user} do
      navigate_to(Routes.user_show_path(@endpoint, :show, user.email))
      :timer.sleep(1000)

      assert page_source() =~ user.uuid
      assert page_source() =~ user.email
    end
  end
end
