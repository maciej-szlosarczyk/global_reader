defmodule GlobalReaderWeb.Browser.IndexTest do
  use GlobalReaderWeb.BrowserCase

  @tag slow: true
  describe "/" do
    test "Has links to other pages" do
      navigate_to(Routes.index_path(@endpoint, :index))

      assert find_element(:link_text, "Create user")
      assert find_element(:link_text, "Sign in")
      assert find_element(:link_text, "Reset password")
    end
  end
end
