ExUnit.configure(exclude: [external: true, slow: true])
ExUnit.start()
Ecto.Adapters.SQL.Sandbox.mode(GlobalReader.Repo, :manual)

# Ensure that HTTP requests never leave the test box
Mox.defmock(GlobalReader.HttpClientMock, for: GlobalReader.HttpClientBehaviour)
