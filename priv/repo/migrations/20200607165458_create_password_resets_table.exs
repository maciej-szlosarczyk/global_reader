defmodule GlobalReader.Repo.Migrations.CreatePasswordResetsTable do
  use Ecto.Migration

  def change do
    create table("password_resets") do
      add(:user_uuid, references("users", column: :uuid, type: :binary_id))
      add(:valid_until, :utc_datetime, null: false)
      add(:used_at, :utc_datetime)

      add(:uuid, :binary_id, null: false, default: fragment("uuid_generate_v4()"))
      timestamps(type: :utc_datetime)
    end

    create unique_index("password_resets", [:uuid])
  end
end
