defmodule GlobalReader.Repo.Migrations.CreateUsersTable do
  use Ecto.Migration

  def change do
    create table("users") do
      add(:password_hash, :string, null: false)
      add(:email, :citext, null: false)
      add(:preferred_name, :string, null: false)

      add(:uuid, :binary_id, null: false, default: fragment("uuid_generate_v4()"))
      timestamps(type: :utc_datetime)
    end

    create unique_index("users", [:email])
    create unique_index("users", [:uuid])
  end
end
