defmodule GlobalReader.Repo.Migrations.CreateUuidExtension do
  use Ecto.Migration

  def change do
    execute("CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\"", "DROP EXTENSION \"uuid-ossp\"")
  end
end
